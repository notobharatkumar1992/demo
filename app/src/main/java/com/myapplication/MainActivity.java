package com.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView textView = (TextView) findViewById(R.id.news);
        Spannable spannable = (Spannable) textView.getText();
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        spannable.setSpan(boldSpan, 41, 52, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        SpannableStringBuilder ssb = new SpannableStringBuilder(getResources().getText(R.string.title));

        String[] newsarray = (getResources().getText(R.string.title)).toString().split("\\.", -1);
        Log.d("test", "newsArray" + newsarray[1] + "news => " + newsarray.length);
        for (int i = 0; i < newsarray.length; i++) {
            Log.d("test", "text => " + newsarray[i] + " , " + newsarray[i].length());
            ssb.append(newsarray[i]);
            Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            if (newsarray[i].length() > 0)
                ssb.setSpan(new ImageSpan(smiley), newsarray[i].length() - 1, newsarray[i].length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            Log.d("test", "newsArray => " + newsarray[i] + ", news => " + newsarray.length);
        }
        textView.setText(ssb, TextView.BufferType.SPANNABLE);
//        Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
//        ssb.setSpan(new ImageSpan(smiley), 10, 11, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//
//        ssb.append(getResources().getText(R.string.app_name));
//        smiley = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
//        ssb.setSpan(new ImageSpan(smiley), ssb.length() - 1, ssb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        textView.setText(ssb, TextView.BufferType.SPANNABLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
